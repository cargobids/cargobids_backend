from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save
from django.urls import reverse
from django.contrib.auth import get_user_model

from .utils import unique_slug_generator


STATUS_CHOICES= (
    ('OPEN', "Open"),
    ('CLOSED', "Closed"),
)

class Quote(models.Model):
    slug = models.SlugField(blank=True, null=True, unique=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                null = True, blank = True,related_name='autore')
    title = models.CharField(max_length=120)
    origin = models.CharField(max_length=3)
    destination = models.CharField(max_length=3)
    pieces = models.IntegerField()
    kilos = models.DecimalField(max_digits= 7, decimal_places=2)
    volume = models.DecimalField(max_digits=5, decimal_places=1)
    gencargo = models.BooleanField(default = True)
    stackable = models.BooleanField(default = True)
    rfc = models.DateField()#ready for carriage
    note = models.TextField(max_length=220)
    draft = models.BooleanField(default=False)
    publish = models.DateField(auto_now=False, auto_now_add=False)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    status= models.CharField(max_length=6, choices = STATUS_CHOICES)
    views= models.IntegerField(default= 0)



    def __str__(self):
        return self.title

    class Meta:
        ordering = ["-timestamp", "destination"]    
      
    def get_absolute_url(self):
        return reverse('quote_detail', kwargs={'slug':self.slug})


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(pre_save_post_receiver, sender=Quote)