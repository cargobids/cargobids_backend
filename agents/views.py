from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.urls import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import  UserPassesTestMixin,LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from src.decorators import agent_required,airline_required
from .models import Quote

from django.shortcuts import render, get_object_or_404




class MyQuotesListView(LoginRequiredMixin, ListView):

    model = Quote
    context_object_name = "quotes_list"
    template_name= 'agents/quotes_list.html'
   
    def get_context_data(self, **kwargs):
        context = super(MyQuotesListView, self).get_context_data(**kwargs)
        context['my_open_quotes'] = Quote.objects.filter(author= self.request.user)|Quote.objects.filter(status='open')
        return context


#DETAIL
class QuoteDetailView(LoginRequiredMixin,UserPassesTestMixin,DetailView):
    model = Quote
    template_name = 'agents/quote_detail.html'
    context_object_name = "quote_detail"
   

    def dispatch(self, request, *args, **kwargs):
        obj= self.get_object()
        try:
            if self.request.user.is_agent:
                raise PermissionDenied
        except:
            obj.author == self.request.user
        return super().dispatch(request, *args, **kwargs)






#UPDATE
class QuoteUpdateView(LoginRequiredMixin,UserPassesTestMixin,UpdateView):
    model = Quote
    fields = ('title','origin','destination',)
    template_name = 'agents/quote_edit.html'
   
    def dispatch(self, request, *args, **kwargs):
        obj= self.get_object()
        if obj.author != self.request.user:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

#DELETE
class QuoteDeleteView(LoginRequiredMixin, UserPassesTestMixin,  DeleteView):
    model = Quote
    template_name = 'agents/quote_delete.html'
    # context_object_name = "quote"
    success_url = reverse_lazy('quotes_list')

    def test_func(self):
        obj= self.get_object()
        return obj.author == self.request.user


# @method_decorator([login_required, agent_required,], name='dispatch')
class QuoteCreateView(CreateView,):
    model = Quote
    template_name = 'agents/quote_new.html'

   
    fields = ('title','origin','destination','pieces', 'kilos', 
              'volume','gencargo', 'stackable','rfc',
               'note', 'draft','publish', 'status')
    def form_valid(self, form):
        form.instance.author= self.request.user
        return super().form_valid(form)


