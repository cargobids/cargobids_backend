from django.urls import path
from .views import (
        MyQuotesListView,
        QuoteDetailView,
        QuoteUpdateView,
        QuoteDeleteView,
        QuoteCreateView,
        
      
)


urlpatterns = [
    path('quotes_list', MyQuotesListView.as_view(), name='my_quotes_list'),
    # path('<slug:slug>', QuoteDetailView.as_view(), name='quote_detail'),
    path('<slug:slug>/edit/', QuoteUpdateView.as_view(), name='quote_update'),
    path('<slug:slug>/delete/', QuoteDeleteView.as_view(), name='quote_delete'),
    path('new/', QuoteCreateView.as_view(), name='quote_new'),
    
]
