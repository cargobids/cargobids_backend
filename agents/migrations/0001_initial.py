# Generated by Django 2.2 on 2019-11-21 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Quote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(blank=True, null=True, unique=True)),
                ('title', models.CharField(max_length=120)),
                ('origin', models.CharField(max_length=3)),
                ('destination', models.CharField(max_length=3)),
                ('pieces', models.IntegerField()),
                ('kilos', models.DecimalField(decimal_places=2, max_digits=7)),
                ('volume', models.DecimalField(decimal_places=1, max_digits=5)),
                ('gencargo', models.BooleanField(default=True)),
                ('stackable', models.BooleanField(default=True)),
                ('rfc', models.DateField()),
                ('note', models.TextField(max_length=220)),
                ('draft', models.BooleanField(default=False)),
                ('publish', models.DateField()),
                ('updated', models.DateTimeField(auto_now=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(choices=[('OPEN', 'Open'), ('CLOSED', 'Closed')], max_length=5)),
            ],
            options={
                'ordering': ['-timestamp', 'destination'],
            },
        ),
    ]
