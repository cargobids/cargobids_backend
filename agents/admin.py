from django.contrib import admin
from airlines.models import Bid
from .models import Quote



class BidsInline(admin.TabularInline):
    model = Bid
    

class QuoteAdmin(admin.ModelAdmin):
    inlines =[
        BidsInline
    ]
    list_display=['publish', 'title', 'origin', 'destination','pieces', 'kilos', 'volume',
                   'rfc',  'status']



admin.site.register(Quote, QuoteAdmin)
