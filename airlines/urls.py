from django.urls import path

from .views import (
        QuoteListView,
        QuoteDetail,
        # QuoteUpdateView,
        # QuoteDeleteView,
        BidCreateView,
      
)


urlpatterns = [
     path('quotes', QuoteListView.as_view(), name='quotes'),
     path('<slug:slug>', QuoteDetail.as_view(), name='al_quote_detail'),
    # path('<slug:slug>/edit/', QuoteUpdateView.as_view(), name='quote_update'),
    # path('<slug:slug>/delete/', QuoteDeleteView.as_view(), name='quote_delete'),
    path('new_bid/', BidCreateView.as_view(), name='bid_new'),

]


