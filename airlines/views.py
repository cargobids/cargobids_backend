from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.urls import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from src.decorators import airline_required
from agents.models import Quote
from .models import Bid




# @method_decorator(airline_required, name='dispatch')
class QuoteListView(LoginRequiredMixin, ListView):

    model = Quote
    template_name= 'airlines/quotes.html'
    
    def get_context_data(self, **kwargs):
        context = super(QuotesListView, self).get_context_data(**kwargs)
        context['open_quotes'] = Quote.objects.filter(status='open')
        return context



# @method_decorator([login_required, airline_required,], name='dispatch')
class QuoteDetail(DetailView):
    model = Quote
    template_name = 'airlines/al_quote_detail.html'
    context_object_name = "quote_detail"
   

class BidCreateView(LoginRequiredMixin, CreateView):
    model = Bid
    template_name = 'airline/bid_new.html'
    context_object_name = "bid"
    login_url= 'accounts/login'

    fields = ('author','airline','rate','all_in', 'surcharges', 
                'cw_required','gencargo', 'conditions','timestamp',
                )
    
    success_url= reverse_lazy("new_bid")


    def form_valid(self, form):
       form.instance.author= self.request.user
       return super().form_valid(form)


