from django.db import models
from agents.models import Quote
from django.urls import reverse
from django.contrib.auth import get_user_model
import uuid


STATUS= (
	('AI', 'ALLIN'),
	('SC', 'SURCHARGES')
	)


class Bid(models.Model):
	id = models.UUIDField(
		primary_key = True,
		default = uuid.uuid4,
		editable=False
		)
	quote = models.ForeignKey(Quote, on_delete = models.DO_NOTHING, related_name='bids')
	author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,
	                            null = True, blank = True,related_name='airline_staff')
	airline = models.CharField(max_length = 2, null = False, blank = False)
	rate = models.DecimalField(max_digits = 4, decimal_places= 2, null = False, blank = False)
	all_in= models.CharField(max_length= 2, choices= STATUS,default= 'ALLIN')
	surcharges= models.DecimalField(max_digits=4, decimal_places =2,null = True, blank = True)
	cw_required= models.DecimalField(max_digits=4, decimal_places =2,null = True, blank = True)
	validity_days= models.CharField(max_length = 20, null = True, blank = True)
	conditions= models.CharField(max_length = 120,null = True, blank = True)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)


	def __str__(self):
		return self.airline
