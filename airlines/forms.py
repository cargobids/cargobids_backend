
from django import forms
from django.core.mail import send_mail
import logging


logger= logging.getLogger(__name__)

class AirlineSignupForm(forms.Form):
    email = forms.EmailField(max_length=30, label='Email', required = True)
    first_name = forms.CharField(max_length=30, label='Nome', required = True)
    last_name = forms.CharField(max_length=30, label='Cognome', required= True)
    company = forms.CharField(max_length=30, label='Azienda', required= True)
    branch = forms.CharField(max_length=30, label='Filiale', required= True)
    code = forms.CharField(max_length=3, label='Codice Vettore', required= False)
    gsa = forms.BooleanField(label='GSA', required= False, widget=forms.CheckboxInput)
    message = forms.CharField(max_length= 300, widget=forms.Textarea,required= False)

    def send_mail(self):
        logger.info("Sending Registration Request form")
        message= "From:Nome {0} {1}\nEmail:{2}\nAzienda: {3} -Filiale: {4}\nCodice:{5}\n {6}\nMessaggio {7}".format(
                    self.cleaned_data['first_name'],
                     self.cleaned_data['last_name'],
                      self.cleaned_data['email'],
                      self.cleaned_data['company'],                     
                       self.cleaned_data['branch'],
                        self.cleaned_data['code'],
                         self.cleaned_data['gsa'],
                         self.cleaned_data['message'],
        )
        send_mail(
            "Registration Request",
            message,
            "registration@cargobids.com",
            ['to@example.com'],
            fail_silently=False,
            )

   

