from django.core.management.base import BaseCommand
import os


class Command(BaseCommand):
    help = 'Renames a Django project'
    def add_arguments(self, parser):
        parser.add_argument('new_projectname', type = str, help = 'the new project name')


    def handle(self, *args, **kwargs):
        new_projectname = kwargs ('new_project_name')

        #apriamo specifici files dove vogliamo cambiare il nome del progetto e modifichiamoli

        files_to_rename = ['demo/settings/base.py', 'demo.wsgi.py', 'manage.py']
        folder_to_rename = 'demo'

        for f in files_to_rename:
            with open(f, 'r') as file:
                filedata = file.read()
            filedata = filedata.replace('demo', new_projectname)

            with open (f, 'w') as file:
                file.write(filedata)

        os.rename(folder_to_rename, new_projectname)

        self.stdout.write(self.style.SUCCESS(f'Project has been renamed to {new_projectname}!'))
