from agents.models import Quote
from airlines.models import Bid


def quotes(request):
    return {'quotes' : Quote.objects.all()}

def bids(request):
    return {'bids': Bid.objects.all()}