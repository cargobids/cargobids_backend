import os
from decouple import config

BASE_DIR = os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))

SECRET_KEY = config('SECRET_KEY')


INSTALLED_APPS = [
  
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'crispy_forms',
    'stripe',

 #DJANGO ALLAUTH https://www.youtube.com/watch?v=mmHn5XYlgto&index=17&list=WL&t=0s
    #https://django-allauth.readthedocs.io/en/latest/providers.html

    'allauth',
    'allauth.account',

    #my apps
    'core',
    'profiles.apps.ProfilesConfig',
 
    'plans.apps.PlansConfig',
    'agents.apps.AgentsConfig',
    'airlines.apps.AirlinesConfig',


   

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'src.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'src.context_processors.quotes',
                'src.context_processors.bids'
            ],
        },
    },
]

WSGI_APPLICATION = 'src.wsgi.application'

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)



CRISPY_TEMPLATE_PACK = 'bootstrap4'

# AUTHENTICATION_BACKENDS = (
#    'django.contrib.auth.backends.ModelBackend',
#    'allauth.account.auth_backends.AuthenticationBackend',
#
# )


STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "Mystatic"),
]

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_cdn", "static_root")


MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_cdn", "media_root")


SITE_ID = 1
#django allauth
STRIPE_PUB_SECRET_KEY = 'pk_test_xTadTvfT6qDTRZnGwgnmUu7I00BoF1Y7dK'
STRIPE_SECRET_KEY = 'sk_test_0vWynfFPmuZaaD9roponbLRn00YZwZHtTS'
AUTH_USER_MODEL = 'profiles.UserProfile'

#django allauth

AUTHENTICATION_BACKENDS = (
  
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
)


EMAIL_BACKEND='django.core.mail.backends.console.EmailBackend'


 
LOGIN_REDIRECT_URL = "join"
ACCOUNT_LOGOUT_REDIRECT_URL = "home"
ACCOUNT_SESSION_REMEMBER= True
DEFAULT_FROM_EMAIL= "admin@cargobids.com"
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS=5
ACCOUNT_EMAIL_CONFIRMATION_HMAC=True
ACCOUNT_EMAIL_VERIFICATION ='mandatory'
ACCOUNT_EMAIL_SUBJECT_PREFIX ='[Site]'
ACCOUNT_LOGIN_ATTEMPTS_LIMIT =5
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION =False
ACCOUNT_LOGOUT_ON_PASSWORD_CHANGE =True
ACCOUNT_SESSION_REMEMBER = False
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_USERNAME_REQUIRED = False # new
ACCOUNT_AUTHENTICATION_METHOD = 'email' # new
ACCOUNT_EMAIL_REQUIRED = True # new
ACCOUNT_UNIQUE_EMAIL = True # new

