from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from plans import views
from django.views.generic import TemplateView

admin.site.site_header = "CargoBids Admin"
admin.site.site_title = "CargoBids Admin Portal"
admin.site.index_title = "Welcome to CargoBids"

urlpatterns = [
	path('admin/', admin.site.urls),
	path('accounts/', include('allauth.urls')),
    path('', TemplateView.as_view(template_name='home.html'),name='home'),
    
     path('plans/', include('plans.urls')),
    path('agents/', include('agents.urls')),
    path('airlines/', include('airlines.urls')),



]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]

elif settings.DEBUG:
	urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
