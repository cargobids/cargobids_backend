from django.contrib import admin
from .models import UserProfile




class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('email', 'firstname', 'lastname', 'company',
            'is_airline', 'is_agent','is_active')
    list_filter = ("email", "company", 'is_agent', "is_airline")
    actions = ["is_active"]
    
    date_hierarchy = 'created' #crea una data filtro in alto alla lista
    readonly_fields = ["created"] #rende il valore non editable
   

list_per_page = 250

admin.site.register(UserProfile, UserProfileAdmin)
