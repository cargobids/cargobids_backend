from django.db import models
from django.contrib.auth.models import AbstractUser, PermissionsMixin





class UserProfile(AbstractUser):
	# username = models.CharField(max_length=25,blank = True, unique = True)
    email = models.EmailField(max_length= 255, unique = True)
    firstname = models.CharField(max_length= 25)
    lastname = models.CharField(max_length=25)
    company = models.CharField(max_length=255)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_airline = models.BooleanField(default=False)
    is_agent = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

	
    def __str__(self):
      return f'{self.email} - {self.company}'



# class Company(models.Model):
#     company = models.CharField(max_length=255)
#     company_staff= models.ForeignKey(UserProfile, on_delete= models.CASCADE,related_name='company_staff')
#     branch= models.CharField(max_length=50)
#     code= models.CharField(max_length=20)
#     logo = models.ImageField(upload_to="logos", blank= True)

#     class Meta:
#         verbose_name_plural = 'Companies'

#     def __str__(self):
#         return self.company