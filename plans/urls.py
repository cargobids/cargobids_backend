from django.contrib import admin
from django.urls import path,include
from plans import views

urlpatterns = [
	# path('home', views.home, name='home'),
    path('<int:pk>', views.plan, name='plan'),
    # path('accounts/', include('django.contrib.auth.urls')),
    path('plan', views.plan, name='plan'),
    path('join', views.join, name='join'),
    path('checkout', views.checkout, name='checkout'),
	# path('accounts/signup', views.SignUp.as_view(), name='signup'),
    path('accounts/settings', views.settings, name='settings'),
	path('updateaccounts', views.updateaccounts, name='updateaccounts'),
]

