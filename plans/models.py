from django.db import models
from django.conf import settings

class MembershipPlan(models.Model):
	title = models.CharField(max_length = 40)
	text = models.TextField()
	premium = models.BooleanField(default = True)


class Customer(models.Model):
	user = models.OneToOneField(
		settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

	stripe_id= models.CharField(max_length=255)
	stripe_subsrciption_id=models.CharField(max_length=255)
	cancel_at_period_end= models.BooleanField(default = False)
	membership = models.BooleanField(default = False)

	def __str__(self):
		return f'{self.user.email} - {self.stripe_id}'