from django.contrib import admin

from .models import MembershipPlan, Customer

admin.site.ste_header= 'CargoBids Admin Panel'

class CustomerAdmin(admin.ModelAdmin):
    list_display =('stripe_id','stripe_subsrciption_id','cancel_at_period_end','membership')
    list_filter = ('stripe_id', 'cancel_at_period_end')

admin.site.register(MembershipPlan)
admin.site.register(Customer,CustomerAdmin)
